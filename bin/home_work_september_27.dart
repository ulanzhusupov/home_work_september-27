import 'dart:io';

void main() {
  // task1();
  // task2();
  // task3();
}

//Напишите программу, которая принимает сумму дохода и определяет
//процент налога на доход в зависимости от суммы. Например,
//доход до $10,000 не облагается налогом, доход от $10,001
//до $50,000 облагается 10% налогом, и свыше 50000 налог - 12%;
void task1() {
  print("Введите ваш доход:");
  int cash = int.tryParse(stdin.readLineSync()!) ?? 0;

  if (cash > 10000 && cash < 50001) {
    double tax = cash * 0.1;
    print("Налог 10% = $tax\$. Ваш остаток = ${cash - tax}\$");
  } else if (cash > 50000) {
    double tax = cash * 0.12;
    print("Налог 12% = $tax\$. Ваш остаток = ${cash - tax}\$");
  } else {
    print("Ваша сумма $cash\$ не облагается налогом");
  }
}

//Создайте калькулятор, который выполняет математические операции
//(сложение, вычитание, умножение, деление) в зависимости от оператора,
//введенного пользователем.
void task2() {
  print("Введите первое число:");
  int a = int.tryParse(stdin.readLineSync()!) ?? 0;

  print("Введите второе число:");
  int b = int.tryParse(stdin.readLineSync()!) ?? 0;

  print("Введите операцию над этими числами (+-*/):");
  String oper = stdin.readLineSync() ?? '';

  switch (oper) {
    case '+':
      {
        print("$a + $b = ${a + b}");
      }
    case '-':
      {
        print("$a - $b = ${a - b}");
      }
    case '*':
      {
        print("$a * $b = ${a * b}");
      }
    case '/':
      {
        print("$a / $b = ${a / b}");
      }
    default:
      {
        print("Введите корректную операцию! Начните сначала.");
      }
  }
}

// Программа принимает расширение файла (например, ".txt", ".jpg", ".pdf") и
//определяет его тип (текстовый, изображение, документ и так далее)
//с использованием конструкции switch case.
void task3() {
  print("Введите расширение файла:");
  String file = stdin.readLineSync() ?? '';

  switch (file) {
    case '.txt':
      {
        print("Это текстовый файл");
      }
    case '.jpg':
      {
        print("Это изображение");
      }
    case '.pdf':
      {
        print("Это документ pdf");
      }
    case '.dart':
      {
        print("Это файл dart");
      }
    case '.xlsx':
      {
        print("Это файл таблицы Microsoft Excel");
      }
    case '.docx':
      {
        print("Это Microsoft Word файл");
      }
    default:
      {
        print("Неверный формат файла! Начните сначала.");
      }
  }
}
